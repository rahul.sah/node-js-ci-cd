const express = require('express');
const path = require('path');
const hbs = require('hbs');
const app = express();

//Define paths for express config
const publicDirectoryPath = path.join(__dirname, '../public');
const viewPath = path.join(__dirname,'../templates/views');
const partialsPath = path.join(__dirname,'../templates/partials');


//Setup handlers bar and engine views locations
app.set('view engine', 'hbs');
app.set('views',viewPath);
hbs.registerPartials(partialsPath);

//Set up static directory to server
app.use(express.static(publicDirectoryPath))


app.get('', (req, res) => { 
    res.render('index', {
        title: 'Weather App',
        name: 'Rahul Sah'
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About Me',
        name: "Rahul Sah"
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        title: "Help",
        name: "Rahul Sah"
    });
});

app.get('/weather', (req, res) => {
    var obj = { forecast: "", location: "" };
    res.send(obj);
});

//app.com
//app.com/help
//app.com/about

app.listen(3001, function () {
    console.log("Server is up at port no 3000")
});