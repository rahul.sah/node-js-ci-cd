# Delete the old repo
echo "Entered the ec2 instance"
sudo rm -rf /home/ec2-user/node-js-ci-cd/
# clone the repo again
git clone https://gitlab.com/rahul.sah/node-js-ci-cd.git
#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
sudo npm install pm2 -g
# starting pm2 daemon
pm2 status
cd /home/ec2-user/node-js-ci-cd
pm2 stop src/app.js
#install npm packages
echo "Running npm install"
npm install
#Restart the node server
pm2 start src/app.js